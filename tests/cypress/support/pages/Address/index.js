const el = require('./elements').ELEMENTS
class Address {
    delivery_address(){
        cy.get(el.addrres_checkbox).should('be.checked');
        cy.get(el.address_select).should('have.value', el.select_id).contains(el.address_name_select);
        cy.get(el.address_card);
        cy.contains(el.user_name);
        cy.contains(el.addres_name);
        cy.contains(el.country);
        cy.contains(el.zipcode);
        cy.get(el.address_btn).click()
    }
}

export default new Address();