export const ELEMENTS = {
    order_content: '.order_carrier_content',
    shipping_price: '$2.00',
    agreement: '#cgv',
    proceed_btn: '.cart_navigation > .button > span',
    shipping_method: '#delivery_option_696066_0'
}