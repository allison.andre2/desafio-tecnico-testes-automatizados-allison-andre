const el = require('./elements').ELEMENTS
class Shipping {
    shipping(){
        cy.get(el.order_content).should('be.visible');
        cy.contains(el.shipping_price);
        cy.get(el.shipping_method).should('be.checked');
        cy.get(el.agreement).check();
        cy.get(el.proceed_btn).click();
    }
}

export default new Shipping();