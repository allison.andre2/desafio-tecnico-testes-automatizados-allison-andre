const el = require('./elements').ELEMENTS
class Payment {
    bank_wire(){
        cy.get(el.payment_block).should('be.visible');
        cy.contains(el.dress_name);
        cy.contains(el.dress_status);
        cy.contains(el.drees_price);
        cy.contains(el.drees_sh_price);
        cy.contains(el.color_size);
        cy.contains(el.tax_price);
        cy.contains(el.total_price);
        cy.get(el.cheque).should('be.visible');
        cy.get(el.bank_wire).should('be.visible').click();
    }

    by_check(){
        cy.get(el.payment_block).should('be.visible');
        cy.contains(el.dress_name);
        cy.contains(el.dress_status);
        cy.contains(el.drees_price);
        cy.contains(el.drees_sh_price);
        cy.contains(el.color_size);
        cy.contains(el.tax_price);
        cy.contains(el.total_price);
        cy.get(el.bank_wire).should('be.visible');
        cy.get(el.cheque).should('be.visible').click();
    }

    confirm_bank(){
        cy.get(el.confirm_colum).should('be.visible');
        cy.contains(el.payment_title_bank);
        cy.contains(el.payment_choose_msg);
        cy.contains(el.total_amount_msg);
        cy.get(el.amount).contains(el.total_price);
        cy.contains(el.currency_msg);
        cy.get(el.confirm_btn).should('be.visible').click();
    }

    confirm_by_check(){
        cy.get(el.confirm_colum).should('be.visible');
        cy.contains(el.payment_title_by_check);
        cy.contains(el.total_amount_msg);
        cy.get(el.amount).contains(el.total_price);
        cy.get(el.confirm_btn).should('be.visible').click();
    }

    order_confirmation_bank(){
        cy.get(el.confirm_colum).should('be.visible');
        cy.get(el.bank_indent).contains(el.order_complete_msg);
        cy.get(el.order_confirmation).contains(el.total_price);
    }

    order_confirmation_by_check(){
        cy.get(el.confirm_colum).should('be.visible');
        cy.get(el.check_alert).should('be.visible').contains(el.order_complete_msg);
        cy.get(el.order_confirmation).contains(el.total_price);
    }
}

export default new Payment();