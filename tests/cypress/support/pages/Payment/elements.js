export const ELEMENTS = {
    payment_block:'.paiement_block',
    dress_name: 'Printed Dress',
    dress_status: 'In stock',
    drees_price: '$26.00',
    drees_sh_price: '$28.00',
    color_size: 'Color : Orange, Size : S',
    tax_price: '$1.12',
    total_price: '$29.12',
    bank_wire:'.bankwire',
    cheque:'.cheque',

    confirm_colum:'#center_column',
    payment_title_bank: ' Bank-wire payment.',
    payment_title_by_check: 'Check payment',
    payment_choose_msg: ' You have chosen to pay by bank wire. Here is a short summary of your order: ',
    total_amount_msg:' - The total amount of your order comes to: ',
    amount:'#amount',
    confirm_btn: '#cart_navigation > .button > span',
    order_complete_msg: 'Your order on My Store is complete.',
    currency_msg: 'Dollar',
    check_alert: '.alert',
    order_confirmation: '.price > strong',
    bank_indent: '.cheque-indent > .dark'

}