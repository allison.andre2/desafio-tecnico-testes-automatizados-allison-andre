export const ELEMENTS = {
    shop_item: '.product_list',
    item_status: '.available-now',
    item_mouseover: '.product_img_link > .replace-2x',
    add_btn: '.ajax_add_to_cart_button > span',
    proceed_container_btn: '.button-container > .button-medium > span',
    dress_name: 'Printed Dress',
    status: 'In stock',
    title: 'Casual Dresses - My Store'
}