const el = require('./elements').ELEMENTS

class Dress {
    go(){
        cy.visit('http://automationpractice.com/index.php?id_category=9&controller=category');
        cy.url().should('eq','http://automationpractice.com/index.php?id_category=9&controller=category');
        cy.title().should('eq', el.title);
    }

    product(){
        cy.get(el.shop_item).should('be.visible').contains(el.dress_name);
    }

    avaiable(){
        cy.get(el.item_status).should('be.visible').contains(el.status);
    }

}

export default new Dress();