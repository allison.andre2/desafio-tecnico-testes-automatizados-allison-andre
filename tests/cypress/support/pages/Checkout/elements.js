export const ELEMENTS = {
    item_mouseover: '.product_img_link > .replace-2x',
    add_btn: '.ajax_add_to_cart_button > span',
    proceed_container_btn: '.button-container > .button-medium > span',
    cart_checkout: '#center_column',
    dress_name: 'Printed Dress',
    dress_status: 'In stock',
    drees_price: '$26.00',
    drees_sh_price: '$28.00',
    color_size: 'Color : Orange, Size : S',
    tax_price: '$1.12',
    total_price: '$29.12',
    owner_name: 'teste teste',
    addres_name: 'teste, Alabama 12345',
    country: 'United States',
    zipcode:  '123456789'

}