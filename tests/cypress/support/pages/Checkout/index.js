const el = require('./elements').ELEMENTS

class Checkout {
    add_cart(){
        cy.get(el.item_mouseover).trigger('mouseover');
        cy.get(el.add_btn).click()
        cy.get(el.proceed_container_btn).click({force: true})
    }

    checkout_verify(){
        cy.get(el.cart_checkout);
        cy.contains(el.dress_name);
        cy.contains(el.dress_status);
        cy.contains(el.drees_price);
        cy.contains(el.drees_sh_price);
        cy.contains(el.color_size);
        cy.contains(el.tax_price);
        cy.contains(el.total_price);
        
        //endereço
        cy.contains(el.owner_name)
        cy.contains(el.addres_name)
        cy.contains(el.country)
        cy.contains(el.zipcode)
    }

    finish_checkout(){
        cy.get('.cart_navigation > .button > span').click()
    }
}

export default new Checkout();