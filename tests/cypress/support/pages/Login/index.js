const el = require('./elements').ELEMENTS

class Logon {
    Login(){
        cy.get(el.signin).click();
        cy.get(el.loginform).should('be.visible');
        cy.get(el.email_locator).type(el.email);
        cy.get(el.passwd_locator).type(el.passwd);
        cy.get(el.submitlogin).click();
        cy.get(el.username).contains('teste teste');
        cy.get(el.dress_over).invoke('css', 'display', 'block');
        cy.get(el.casual_dressbtn).click()
    }
}

export default new Logon();