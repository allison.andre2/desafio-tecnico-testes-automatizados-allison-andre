export const ELEMENTS = {
    signin: '.login',
    loginform: '#login_form',
    email_locator: '#email',
    passwd_locator: '#passwd',
    submitlogin: '#SubmitLogin > span',
    username: '.account > span',
    dress_over: 'ul[class="submenu-container clearfix first-in-line-xs"]',
    casual_dressbtn: ':nth-child(2) > .submenu-container > :nth-child(1) > a',
    passwd: '123456',
    email: 'teste-desafio@gmail.com'
}