/// <reference types="cypress"/>


import Dress from '../support/pages/Dresses'
import Checkout from '../support/pages/Checkout'
import Logon from '../support/pages/Login'
import Address from '../support/pages/Address'
import Shipping from '../support/pages/Shipping'
import Payment from '../support/pages/Payment'

describe('Suite de teste compra do vestido', () => {

    it('Validar fluxo de compra do vestido com o método de pagamento via transferencia bancaria', () => {
        //Obs: Por algum motivo sem explicação ao colocar as funções em outros it a automação quebra, solução encontrada foi deixar no mesmo it e definir um unico fluxo de teste.
        // O ideal seria separar por its para melhor visualização e manutenção. 
        //visita a paágina que contém o vesstido.
        Dress.go();

        //Realizar o login
        Logon.Login();

        //Validar a disponibilidade do produto
        Dress.product();
        Dress.avaiable();
        
        //Validar os dados do carrinho e finaliza o carrinho
        Checkout.add_cart();
        Checkout.checkout_verify();
        Checkout.finish_checkout();
        
        //Validar o endereco do carrinho
        Address.delivery_address();
        
        //Validar o método de envio
        Shipping.shipping();

        //seleciona o método de pagamento
        Payment.bank_wire();
        //Confirma a compra
        Payment.confirm_bank();
        // Confirma a ordem
        Payment.order_confirmation_bank();

    });

    it('Validar fluxo de compra do vestido com o método de pagamento via cheque', () => {
        //visita a paágina que contém o vesstido
        Dress.go();

        //Realizar o login
        Logon.Login();

        //Validar a disponibilidade do produto
        Dress.product();
        Dress.avaiable();
        
        //Validar os dados do carrinho e finaliza o carrinho
        Checkout.add_cart();
        Checkout.checkout_verify();
        Checkout.finish_checkout();
        
        //Validar o endereco do carrinho
        Address.delivery_address();
        
        //Validar o método de envio
        Shipping.shipping();

        //seleciona o método de pagamento
        Payment.by_check();

        //Confirma a compra
        Payment.confirm_by_check();

        //confirma a ordem
        Payment.order_confirmation_by_check();
    });

});