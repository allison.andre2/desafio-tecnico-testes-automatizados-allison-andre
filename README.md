# Desafio Técnico – Testes Automatizados – Allison André.

Desafio técnico automação da aplicação http://automationpractice.com/ realizando a compra de um vestido.

## Description
Automação realizada com o framework **cypress**, para gerar os relatorios foi utilizado o **framework allure**.
**Obs.** Pelo fato de a aplicação ser instável o teste pode falhar, caso aconteça bastar rodar o teste novamente.

Para abrir o relatório do projeto basta digitar no terminal allure open, caso não abra, prosseguir para a etapa de instalação.

## Installation
Para executar é preciso clonar o repositório e ter o **cypress** , **node.js**, **java** a partir da versão 8, **framework allure** e o **scoop** instalado.

Para instalar o scoop é necessário abrir um terminal power shell do windows e digitar o seguinte comando: **Set-ExecutionPolicy RemoteSigned -Scope CurrentUser # Optional: Needed to run a remote script the first time**
E depois o seguinte comando: **irm get.scoop.sh | iex**

Agora é preciso instalar a allure framework digitando o seguinte comando: **scoop install allure**.

O próximo passo é instalar allure plugin dentro da pasta do prejeto cd tests/ e executar o seguinte comando **npm i -D @shelex/cypress-allure-plugin**

## Usage
Após instalar o plugin é só executar o cypress com o comando  **npx cypress run --env allure=true**

Para gerar o relatório digitar no terminal **allure generate**

Para abrir o relatório **allure open**

## Authors and acknowledgment
Allison André


